package exercice4;

public class Classic extends Employee {
	private int weeklyHours;
	private int rate;
	
	public Classic(String lastname, String firstname, String address, String service, int weeklyHours, int rate) {
		super(lastname, firstname, address, service);
		this.weeklyHours = weeklyHours;
		this.rate = rate;
	}

	@Override
	public int GetMonthlyWage() {
		return weeklyHours * rate * 4;
	}

}
