package exercice4;

public class Exercice4Test {
	
	public static void Test() {
		Classic paul = new Classic("Paul", "Dupuis", "12 rue du piquet", "IT", 35, 15);
		Classic marie = new Classic("Marie", "Dupuis", "12 rue du piquet", "IT", 35, 15);
		Classic jacques = new Classic("Jacques", "Dupont", "8 chemin de la libération", "Compta", 35, 25);
		
		Employee[] joeMen = {paul, marie};
		Manager joe = new Manager("Joe", "Doe", "Chemin de la victoire", "IT", joeMen, 2500);
		
		Employee[] pierreMen = {joe, jacques};
		Manager pierre = new Manager("Pierre", "Baguette", "Tour d'ivoire", "Management", pierreMen, 4000);
		
		Employee[] men = {paul, marie, jacques, joe, pierre};
		Business business = new Business(men);
		
		business.PrintSummary();
	}
	
}
