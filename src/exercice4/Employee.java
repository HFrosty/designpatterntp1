package exercice4;

public abstract class Employee {
	private String lastname;
	private String firstname;
	private String address;
	private String service;
	
	public Employee(String lastname, String firstname, String address, String service) {
		this.lastname = lastname;
		this.firstname = firstname;
		this.address = address;
		this.service = service;
	}
	
	public String GetLastname() {
		return lastname;
	}
	
	public String GetFirstName() {
		return firstname;
	}
	
	public abstract int GetMonthlyWage();
}
