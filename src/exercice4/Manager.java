package exercice4;

public class Manager extends Employee {
	private Employee[] men;
	private int wage;
	
	public Manager(String lastname, String firstname, String address, String service, Employee[] men, int wage) {
		super(lastname, firstname, address, service);
		this.men = men;
		this.wage = wage;
	}

	@Override
	public int GetMonthlyWage() {
		return wage;
	}
	
	public Employee[] GetMen() {
		return men;
	}

}
