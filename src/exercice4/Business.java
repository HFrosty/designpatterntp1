package exercice4;

public class Business {
	private Employee[] employees;
	
	public Business(Employee[] employees) {
		this.employees = employees;
	}
	
	public void PrintSummary() {
		for(int i = 0; i < employees.length; ++i) {
			String message = employees[i].GetLastname() + " " + employees[i].GetFirstName() + " " + employees[i].GetMonthlyWage() + " dollars\n";
			if(employees[i] instanceof Manager) {
				Manager cast = (Manager)employees[i];
				for(int j = 0; j < cast.GetMen().length; ++j) {
					if(j != 0) {
						message += "\n";
					}
					message += "\t" + cast.GetMen()[j].GetLastname() + " " + cast.GetMen()[j].GetFirstName();
				}
				message += "\n";
			}
			System.out.print(message);
		}
	}
}
