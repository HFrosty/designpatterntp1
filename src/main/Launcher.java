package main;

import exercice1.Exercice1Test;
import exercice2.Exercice2Test;
import exercice3.Exercice3Test;
import exercice4.Exercice4Test;
import exercice5.Exercice5Test;

public class Launcher {

	public static void main(String[] args) {
		//Exercice1Test.Test();
		//Exercice2Test.Test();
		//Exercice3Test.Test();
		//Exercice4Test.Test();
		Exercice5Test.Test();
	}
	
}
