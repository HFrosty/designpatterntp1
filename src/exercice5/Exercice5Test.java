package exercice5;

public class Exercice5Test {

	public static void Test() {
		HTML htmldoc = new HTML();
		Text textdoc = new Text();
		composantPDF composant = new composantPDF("bilan.pdf", "1.9.18");
		PDF pdfdoc = new PDF(composant);
		
		htmldoc.afficheDocument();
		textdoc.afficheDocument();
		pdfdoc.afficheDocument();
	}
	
}
