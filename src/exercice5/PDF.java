package exercice5;

public class PDF implements Document {
	private composantPDF composant;
	
	public PDF(composantPDF composant) {
		this.composant = composant;
	}
	
	@Override
	public void afficheDocument() {
		composant.afficheNom();
		composant.afficheVersion();
	}

}
