package exercice5;

public class composantPDF {
	private String nom;
	private String version;
	
	public composantPDF(String nom, String version) {
		this.nom = nom;
		this.version = version;
	}
	
	public void afficheNom() {
		System.out.println("Nom: " + nom);
	}
	
	public void afficheVersion() {
		System.out.print("Version: " + version);
	}
	
}
