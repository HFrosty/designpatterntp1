package exercice2;

public class Exercice2Test {

	public static void Test() {
		ComputerUIFactory.Get().BuildButton().PrintDescription();
		ComputerUIFactory.Get().BuildComboBox().PrintDescription();
		ComputerUIFactory.Get().BuildTextZone().PrintDescription();
		System.out.println();
		PhoneUIFactory.Get().BuildButton().PrintDescription();
		PhoneUIFactory.Get().BuildComboBox().PrintDescription();
		PhoneUIFactory.Get().BuildTextZone().PrintDescription();
		System.out.println();
		TabletUIFactory.Get().BuildButton().PrintDescription();
		TabletUIFactory.Get().BuildComboBox().PrintDescription();
		TabletUIFactory.Get().BuildTextZone().PrintDescription();
	}
}
