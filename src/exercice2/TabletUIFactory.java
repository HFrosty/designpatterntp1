package exercice2;

public class TabletUIFactory implements UIFactory {
	private static TabletUIFactory singleton;
	
	public static TabletUIFactory Get() {
		if(singleton == null) {
			singleton = new TabletUIFactory();
		}
		return singleton;
	}
	
	@Override
	public Button BuildButton() {
		return new TabletButton();
	}

	@Override
	public ComboBox BuildComboBox() {
		return new TabletComboBox();
	}

	@Override
	public TextZone BuildTextZone() {
		return new TabletTextZone();
	}

}
