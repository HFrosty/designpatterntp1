package exercice2;

public class ComputerUIFactory implements UIFactory {
	private static ComputerUIFactory singleton;
	
	public static ComputerUIFactory Get() {
		if(singleton == null) {
			singleton = new ComputerUIFactory();
		}
		return singleton;
	}
	
	@Override
	public Button BuildButton() {
		return new ComputerButton();
	}

	@Override
	public ComboBox BuildComboBox() {
		return new ComputerComboBox();
	}

	@Override
	public TextZone BuildTextZone() {
		return new ComputerTextZone();
	}

}
