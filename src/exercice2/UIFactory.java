package exercice2;

public interface UIFactory {	
	public Button BuildButton();
	
	public ComboBox BuildComboBox();
	
	public TextZone BuildTextZone();
}
