package exercice2;

public class PhoneUIFactory implements UIFactory {
	private static PhoneUIFactory singleton;
	
	public static PhoneUIFactory Get() {
		if(singleton == null) {
			singleton = new PhoneUIFactory();
		}
		return singleton;
	}
	
	@Override
	public Button BuildButton() {
		return new PhoneButton();
	}

	@Override
	public ComboBox BuildComboBox() {
		return new PhoneComboBox();
	}

	@Override
	public TextZone BuildTextZone() {
		return new PhoneTextZone();
	}

}
