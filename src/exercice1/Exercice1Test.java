package exercice1;

public class Exercice1Test {

	public static void Test() {
		Car car = new Car();
		Bike bike = new Bike();
		Quad quad = new Quad();
		
		car.Start();
		bike.Start();
		quad.Start();
		
		car.Move();
		bike.Move();
		quad.Move();
	}
	
}
