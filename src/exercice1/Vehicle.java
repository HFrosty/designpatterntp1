package exercice1;

public abstract class Vehicle {

	public void Start() {
		System.out.println("Engine starting to roar");
	}
	
	public abstract void Move();
}
