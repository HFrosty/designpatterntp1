package exercice1;

public class Bike extends Vehicle {

	@Override
	public void Move() {
		System.out.println("The bike is moving");
	}
	
}
