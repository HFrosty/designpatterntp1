package exercice3;

public abstract class BuilderInhabitation {
	protected Inhabitation inhabitation;
	
	public BuilderInhabitation(Inhabitation inhabitation) {
		this.inhabitation = inhabitation;
	}
	
	public void BuildInhabitation() {
		SetWindowCount();
		SetRoomCount();
	}
	
	public abstract void SetWindowCount();
	
	public abstract void SetRoomCount();
}
