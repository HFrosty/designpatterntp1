package exercice3;

public class BuilderHouse extends BuilderInhabitation {

	public BuilderHouse(Inhabitation inhabitation) {
		super(inhabitation);
	}

	@Override
	public void SetWindowCount() {
		inhabitation.SetWindowCount(12);
	}

	@Override
	public void SetRoomCount() {
		inhabitation.SetRoomCount(8);
	}

}
