package exercice3;

import java.util.ArrayList;

public class Exercice3Test {

	public static void Test() {
		String[] owners = {"Paul", "Marie"};
		City paris = new City("Paris", 92000);
		
		Inhabitation inhabitation = new Inhabitation(owners, paris);
		System.out.println("Room count = " + inhabitation.GetRoomCount() + " --- Window count = " + inhabitation.GetWindowCount());
		BuilderInhabitation builder = new BuilderHouse(inhabitation);
		builder.BuildInhabitation();
		System.out.println("Builder is done");
		System.out.println("Room count = " + inhabitation.GetRoomCount() + " --- Window count = " + inhabitation.GetWindowCount());
		
		System.out.println();
		
		inhabitation = new Inhabitation(owners, paris);
		System.out.println("Room count = " + inhabitation.GetRoomCount() + " --- Window count = " + inhabitation.GetWindowCount());
		builder = new BuilderBuilding(inhabitation);
		builder.BuildInhabitation();
		System.out.println("Builder is done");
		System.out.println("Room count = " + inhabitation.GetRoomCount() + " --- Window count = " + inhabitation.GetWindowCount());
	}
	
}
