package exercice3;

import java.util.ArrayList;

public class Inhabitation {
	private String[] owners;
	private int roomCount;
	private int windowCount;
	private City city;
	
	public Inhabitation(String[] owners, City city) {
		this.owners = owners;
		this.city = city;
	}

	public void SetWindowCount(int i) {
		windowCount = i;
	}

	public void SetRoomCount(int i) {
		roomCount = i;
	}
	
	public int GetRoomCount() {
		return roomCount;
	}
	
	public int GetWindowCount() {
		return windowCount;
	}
}
