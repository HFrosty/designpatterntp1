package exercice3;

public class BuilderBuilding extends BuilderInhabitation {

	public BuilderBuilding(Inhabitation inhabitation) {
		super(inhabitation);
	}

	@Override
	public void SetWindowCount() {
		this.inhabitation.SetWindowCount(150);
	}

	@Override
	public void SetRoomCount() {
		this.inhabitation.SetRoomCount(100);
	}

}
