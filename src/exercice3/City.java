package exercice3;

public class City {
	private String name;
	private int zipcode;
	
	public City(String name, int zipcode) {
		this.name = name;
		this.zipcode = zipcode;
	}
}
